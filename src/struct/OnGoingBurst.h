/*
 * Merger.h
 *
 *  Created on: Jul 6, 2012
 *      Author: Jonas Kunze (kunze.jonas@gmail.com)
 */

#ifndef ON_GOING_BURST_H_
#define ON_GOING_BURST_H_

#include <tbb/concurrent_queue.h>
#include <tbb/concurrent_hash_map.h>

#include <zmq.hpp>

namespace na62 {
namespace merger {

// This structure is threadsafe
struct OnGoingBurst {
		OnGoingBurst(uint16_t p_burst_id):
		 burst_id(p_burst_id),
		 created_at(std::time(nullptr)),
		 eob_packets{}
		{
		}
		const double timeDiff() const {
            return std::difftime(std::time(nullptr), created_at);
		}
		
		const uint16_t burst_id;
		const std::time_t created_at;
		tbb::concurrent_queue<zmq::message_t *> event_queue;
		std::vector<char *> eob_packets; // It should be only one but who knows....
};

// A concurrent hash table that maps bursts id to OnGoing burst.
typedef tbb::concurrent_hash_map<uint16_t, std::shared_ptr<OnGoingBurst>> BurstTable;


} /* namespace merger */
} /* namespace na62 */
#endif /* MERGER_NO_CACHE_H_ */
