/*
 * Merger.h
 *
 *  Created on: Jul 5, 2017
 *      Author: Marco Boretto (marco.bore@gmail.com)
 */

#ifndef THREADPOOL_H_
#define THREADPOOL_H_


#include <vector>
#include <thread>
#include <atomic>
#include <unistd.h>
#include <condition_variable>
#include <mutex>
#include <sstream>

#include <tbb/concurrent_queue.h>
#include "../struct/OnGoingBurst.h"


namespace na62 {
namespace merger {

class MergerNoCache;

class ThreadPool {
public:
    ThreadPool();
    ~ThreadPool();
    void start(uint pool_size);
    void stop();
    void pushTask(std::shared_ptr<OnGoingBurst> task);

    void attachData(MergerNoCache * merger_data) {
    	merger_data_ = merger_data;
    }

    class BurstStatistics {
        public:
        std::atomic<uint32_t> sob_time;
        std::atomic<uint32_t> count;
        std::atomic<uint16_t> burst_number;

        BurstStatistics(): sob_time{0}, count{0}, burst_number{0} {

        }
        BurstStatistics(const BurstStatistics &stats):
            sob_time{stats.sob_time.load()},
            count{stats.count.load()},
            burst_number{stats.burst_number.load()}
            {

        }
    };


	std::string getProgressStats() const {
        //making a copy of the vector
        std::vector<BurstStatistics> local_pool_burst_progress(pool_burst_progress_); // Possible operation thanks to the definition of the copy constructor...
        //std::sort(local_pool_burst_progress.begin(), local_pool_burst_progress.end()); //sorting by the sob timestamp
        std::vector<std::pair<uint32_t, uint32_t>> ordered_indeces;
        uint index = 0;
        for (auto & info: local_pool_burst_progress) {
            ordered_indeces.emplace_back(std::make_pair(index++, info.sob_time.load()));
        }

        std::sort(ordered_indeces.begin(), ordered_indeces.end(),
            [](const std::pair<uint32_t, uint32_t>& afirst, const std::pair<uint32_t, uint32_t>& asecond) {
                return afirst.second < asecond.second;
            }
        );
		std::stringstream stream;
        for (auto & index: ordered_indeces) {
			stream << local_pool_burst_progress[index.first].burst_number << ":" << local_pool_burst_progress[index.first].count << ";";
		}



		// for (auto & thread_stats: local_pool_burst_progress) {
		// 	stream << thread_stats.burst_number << ":" << thread_stats.count << ";";
		// }
		return stream.str();
	}

	uint getNumberOfEventsInLastBurst(){
		return event_in_last_burst_;
	}


private:
    uint pool_size_;
    std::vector<std::thread> threads_;
    std::atomic<bool> is_running_;
    std::mutex mutex_;
    std::condition_variable new_job_;
    tbb::concurrent_queue<std::shared_ptr<OnGoingBurst>> job_queue_;
    void drowsiness(uint id);
    void doTask(uint thread_id, uint mytask);




    std::vector<BurstStatistics> pool_burst_progress_;
    uint event_in_last_burst_; //Last burst written by the pool hopefully the most recent one

    MergerNoCache * merger_data_;
    void handleBurstFinished(uint thread_id, uint32_t finishedBurstID);

    void saveBurst(uint thread_id, uint32_t runNumber, uint32_t burstID, uint32_t sobTime);

};

}
}
#endif /* THREADPOOL_H_ */
