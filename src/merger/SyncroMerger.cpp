/*
	2022, Ceoletta Marco
*/
#include "SyncroMerger.h"


#include <structs/Event.h>
#include <storage/BurstFileWriter.h>

namespace na62 {
namespace merger {

SyncroMerger::SyncroMerger() : MergerNoCache::MergerNoCache() {
	LOG_INFO("Starting DIM Syncro Thread");
	BurstInfo* bi = new BurstInfo(this);
	this->bi_ = bi;
}


SyncroMerger::~SyncroMerger() {
}

void SyncroMerger::thread() {
	LOG_INFO("Starting Syncro Merger Thread");

	is_running_ = true;
	while (is_running_) {
		zmq::message_t * event_message = nullptr;
		events_queue_.try_pop(event_message);
		if (event_message == nullptr) {
			continue;
		}
		EVENT_HDR* event = reinterpret_cast<EVENT_HDR*>(event_message->data());

		if (event_message->size() != event->length * 4) {
			LOG_ERROR("Received " << event_message->size() << " Bytes with an event of length " << (event->length * 4) );
			delete event_message;
			continue;
		}

		//condition on the onBurst status
		if(on_Data_coll_) {

			const uint32_t burst_id = event->burstID;

			BurstTable::accessor accessor;
			bool found = table_.find(accessor, burst_id);
			if (not found) {
				table_.insert(accessor, burst_id);
				LOG_INFO("Creating the burst: "  << accessor->first);
				accessor->second = std::shared_ptr<OnGoingBurst>(new OnGoingBurst(burst_id)); 
				burst_write_pool_.pushTask(accessor->second);// Making a copy of the pointer!
			}
			accessor->second->event_queue.push(event_message);
		} else {
			delete event_message;
		}
	}
}

} /* namespace merger */
} /* namespace na62 */
