#include "MergerNoCache.h"

#include <fstream>
#include <iostream>

#include <iomanip>
#include <boost/date_time.hpp>
#include <structs/Event.h>

#include <utils/Utils.h>
#include <storage/BurstFileWriter.h>
#include "../options/MyOptions.h"

namespace na62 {
namespace merger {

MergerNoCache::MergerNoCache():
		eventsInLastBurst_(0), storageDirs_({Options::GetString(OPTION_STORAGE_DIR) + "/", Options::GetString(OPTION_STORAGE_DIR_2) + "/"}),
		lastSeenRunNumber_(0),
		threshold_(Options::GetInt(OPTION_DISK_THRESHOLD)) {

		//Starting EOB Collector thread
		EobThread_ = std::thread{&na62::merger::EobCollector::run, &eobCollector_};

		burst_write_pool_.attachData(this);
		burst_write_pool_.start(Options::GetInt(OPTION_MERGER_THREADS)); //this is the number of threads to start for the merger, made it settable?
}

MergerNoCache::~MergerNoCache() {
}

void MergerNoCache::push(zmq::message_t * event) {
	events_queue_.push(event);
}

void MergerNoCache::thread() {
	LOG_INFO("Starting Merger Thread");
	is_running_ = true;
	while (is_running_) {
		zmq::message_t * event_message = nullptr;
		events_queue_.try_pop(event_message);

		if (event_message == nullptr) {
			continue;
		}

		if (event_message->size() < sizeof(EVENT_HDR)) {
			LOG_ERROR("ZMQ Packet smaller than the event header...");
			delete event_message;
			continue;
		}

		EVENT_HDR * event = reinterpret_cast<EVENT_HDR *>(event_message->data());

		if (event_message->size() != event->length * 4) {
			LOG_ERROR("Received " << event_message->size() << " Bytes with an event of length " << (event->length * 4));
			delete event_message;
			continue;
		}
		const uint32_t burst_id = event->burstID;

        // TODO Maybe worth to cache some events and limit the accessor creating destruction
        BurstTable::accessor accessor;
        bool found = table_.find(accessor, burst_id);
        if (not found) {
            table_.insert(accessor, burst_id);
            LOG_INFO("Creating the burst: "  << accessor->first);
            accessor->second = std::shared_ptr<OnGoingBurst>(new OnGoingBurst(burst_id)); 
			burst_write_pool_.pushTask(accessor->second);// Making a copy of the pointer!
        }
        accessor->second->event_queue.push(event_message);
	}
}

void MergerNoCache::stopPool() {
	burst_write_pool_.stop();
	eobCollector_.stop();
	is_running_ = false;
}

void MergerNoCache::onInterruption() {
	shutdown();
}

void MergerNoCache::shutdown() {
	LOG_INFO("Stopping merger queue pull");
	is_running_ = false;
	LOG_INFO("Stopping write pool");
	burst_write_pool_.stop();
	LOG_INFO("Stopping EobCollector");
	eobCollector_.stop();
	LOG_INFO("Joining EobCollector Thread");
	EobThread_.join();
}

} /* namespace merger */
} /* namespace na62 */
