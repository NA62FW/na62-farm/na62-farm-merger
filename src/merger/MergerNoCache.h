/*
 * Merger.h
 *
 *  Created on: Jul 6, 2012
 *      Author: Jonas Kunze (kunze.jonas@gmail.com)
 */

#ifndef MERGER_NO_CACHE_H_
#define MERGER_NO_CACHE_H_

#include <atomic>
#include <memory>
#include <string>

#include <stddef.h>
#include <stdint.h>

#include <zmq.hpp>

#include <tbb/concurrent_queue.h>
#include <thread>

#include <sys/statvfs.h>

#include <utils/AExecutable.h>

#include "../dim/EobCollector.h"
#include "ThreadPool.h"
#include "../struct/OnGoingBurst.h"


namespace na62 {
namespace merger {


class MergerNoCache: public AExecutable {
public:

	MergerNoCache();
	~MergerNoCache();
	MergerNoCache(const MergerNoCache &m) = delete;
	
	void push(zmq::message_t* event);

	std::string getProgressStats() {
		return burst_write_pool_.getProgressStats();
	}

	uint getNumberOfEventsInLastBurst() {
		return burst_write_pool_.getNumberOfEventsInLastBurst();
	}

	na62::merger::EobCollector& getEOBCollector() {
		return  eobCollector_;
	}

    // Thrown
	double getDiskOccupancy(const std::string & path) const {
	    struct statvfs buffer;
		int ret = statvfs(path.c_str(), &buffer);
		if (not ret) {
			const float total = static_cast<float>(buffer.f_blocks);
			const float available = static_cast<float>(buffer.f_bfree);
			const float used = total - available;
			const float usedPercentage = used / total;
			return usedPercentage * 100;
		}
		throw std::exception();
	}

	std::string getStorageDir() const {
	    try {
	    	//Will attempt to write on the secondary if the threshold is exceeded
	    	double disk_occupancy = getDiskOccupancy(storageDirs_[0]);
	    	if (disk_occupancy < threshold_) { //threshold_ = 80
	    		LOG_INFO("Using primary storage dir" << storageDirs_[0]<< " Occupancy: " << disk_occupancy << " < " << threshold_);
	    		return storageDirs_[0];
	    	}
	    } catch(std::exception e) {
	        LOG_ERROR("Primary storage dir: " << storageDirs_[0] << " doesn't exists");
	    }

	    try {
	    	//Will throw if doesn't exist the second path to write
	    	// In this case the primary path is returned
	    	double disk_occupancy = getDiskOccupancy(storageDirs_[1]);
			LOG_INFO("Using secondary storage dir: " << storageDirs_[1] << " Occupancy: " << disk_occupancy);
			return storageDirs_[1];

		} catch(std::exception e) {
			//LOG_ERROR("Primary storage dir: " << storageDirs_[0] << " doesn't exists");
		}
		LOG_INFO("Using primary storage dir: " << storageDirs_[1]);
		return storageDirs_[0];

	}
	void stopPool();
	void shutdown();


protected:
    friend class ThreadPool;
	void thread();
	void startBurstControlThread(uint32_t& burstID);
	void saveBurst(std::map<uint32_t, zmq::message_t*>& eventByID, uint32_t& runNumber, uint32_t& burstID, uint32_t& sobTime);
	virtual void onInterruption();
	std::string generateFileName(uint32_t sob, uint32_t runNumber, uint32_t burstID, uint32_t duplicate);

	na62::merger::EobCollector eobCollector_;

    BurstTable table_;
	
	std::atomic<bool> is_running_;
	
	uint eventsInLastBurst_;
	const std::array<std::string, 2> storageDirs_;
	uint32_t lastSeenRunNumber_;
	uint const threshold_;
	
	std::map<uint32_t, dim::BurstTimeInfo> burstInfos; // TODO incorporate in OnGoing burst struct

	tbb::concurrent_queue<zmq::message_t*> events_queue_;
	ThreadPool burst_write_pool_;
	std::thread EobThread_;
};

} /* namespace merger */
} /* namespace na62 */
#endif /* MERGER_NO_CACHE_H_ */
