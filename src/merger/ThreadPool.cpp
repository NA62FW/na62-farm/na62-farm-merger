#include <storage/BurstFileWriter.h>
#include <structs/Event.h>

#include "ThreadPool.h"
#include "MergerNoCache.h"
#include "../options/MyOptions.h"



namespace na62 {
namespace merger {

ThreadPool::ThreadPool(): is_running_(false) {
}

void ThreadPool::start(uint pool_size) {
    is_running_ = true;
	pool_size_ = pool_size;

    for (uint id = 0; id < pool_size_; ++id) {
    	pool_burst_progress_.push_back(BurstStatistics());
    }
    for (uint id = 0; id < pool_size_; ++id) {
        threads_.emplace_back(std::thread(&ThreadPool::drowsiness, this, id));
    }
}

void ThreadPool::pushTask(std::shared_ptr<OnGoingBurst> task) {
    job_queue_.push(task);
    new_job_.notify_one(); // Waking up a thread in the pool
}

void ThreadPool::drowsiness(uint thread_id) {
	std::shared_ptr<OnGoingBurst> burst_data = nullptr;
	const uint32_t max_event = Options::GetInt(OPTION_MAX_EVENT_IN_FILE);
	uint32_t run_number = 0;

	while (is_running_) {
		std::unique_lock<std::mutex> lock(mutex_);
		new_job_.wait_for(lock, std::chrono::seconds(5));//Will automatically check for a new job every  
		if (job_queue_.try_pop(burst_data)) {
			LOG_INFO("Doing burst id: " << burst_data->burst_id << " count " << burst_data.use_count());




			// Getting the burst info burst id sob timestamp
			uint32_t sob_time = 0;	
			uint8_t attempt_count = 0;
			while (sob_time == 0) {
				++attempt_count;
				try {
					BurstTimeInfo burstInfo = merger_data_->getEOBCollector().getBurstInfoSOB(burst_data->burst_id);
					run_number = burstInfo.runNumber;
					sob_time = burstInfo.sobTime;
					LOG_INFO("DIM information found! SOB time: " << sob_time << " run number: " << run_number);




					break;
				} catch (std::exception &e) {
					LOG_ERROR("Missing DIM information for burst " << burst_data->burst_id
								<< " in run " <<  run_number << ". This burst will have missing SOB/EOB information!");
				} catch (...) {
					LOG_ERROR("Unknown exception raised from EOB collector during burst" << burst_data->burst_id << " Run number: " << run_number);
				}
				// Give some time to DIM to retrieve the info
				if (attempt_count > 10) {
					if (burst_data->burst_id <= 4) run_number++;
					LOG_ERROR("Giving up.. No informatin found for burst " << burst_data->burst_id
								<< " in run " <<  run_number << ". This burst will have missing SOB/EOB information!");
					break;
				}
				LOG_ERROR("Waiting for DIM  SOB/EOB information!");
				std::this_thread::sleep_for(std::chrono::milliseconds(500));
			}

			pool_burst_progress_[thread_id].burst_number = burst_data->burst_id;
			pool_burst_progress_[thread_id].sob_time = sob_time;

			std::string file_path = BurstFileWriter::generateFilePath(merger_data_->getStorageDir(), sob_time, run_number, burst_data->burst_id, Options::GetInt(OPTION_MERGER_ID));
			LOG_INFO("Writing file " << file_path);

			//constexpr uint32_t max_event = 5000;
			std::unique_ptr<BurstFileWriter> writer = std::unique_ptr<BurstFileWriter>(new BurstFileWriter(file_path, max_event, sob_time, run_number, burst_data->burst_id));
			LOG_INFO("Header initilized with " << max_event << " events starting event write: " << file_path);

			uint32_t event_count = 0;
			uint32_t total_event_count = 0;

			while (is_running_) { // Start popping from the event queue
				zmq::message_t * event_message = nullptr;
				if (burst_data->event_queue.try_pop(event_message)) {
					EVENT_HDR * event = reinterpret_cast<EVENT_HDR *>(event_message->data());

					//Check if it is a EOB
					if (event->getL0TriggerTypeWord() == TRIGGER_L0_EOB) {
						//Extend the EOB event with extra info from DIM
						if (Options::GetInt(OPTION_APPEND_DIM_EOB)) {
							// getting rid of the zmq packet
							char * eob_copy = new char[event->length * 4];
							std::memcpy(eob_copy, static_cast<char *>(event_message->data()), event->length * 4);
							burst_data->eob_packets.push_back(eob_copy);
							delete event_message; // Delete zmq message
							continue; // Skip counting
						}
						// keep on precessing as a normal event..
					}

					++total_event_count;
					// write the event!					
					if (++event_count > max_event) {
						// preallocated event size exceeded..
						// Updating with the correct number of events written, this information will be flushed in the file when the destructor is called.
						//writer->setNumberOfEvents(event_count);
						writer->flushFile(event_count);
						writer.reset(); // Flushing the file

						try {
							if (BurstFileWriter::doChown(file_path, Options::GetString(OPTION_FILE_AUTHOR), Options::GetString(OPTION_FILE_GROUP))) {
								LOG_INFO("Correctly changed permissions to: " << file_path);
							} else {
								LOG_ERROR("Chown failed on file: " << file_path);
							}
						} catch (const std::exception & exception) {
							LOG_ERROR("Chown Failed " << exception.what() << " filename: " << file_path);
						}

						// Opening a new file
						file_path = BurstFileWriter::generateFilePath(merger_data_->getStorageDir(), sob_time, run_number, burst_data->burst_id, Options::GetInt(OPTION_MERGER_ID));
						LOG_INFO("Opening new file " << file_path);
						writer = std::unique_ptr<BurstFileWriter>(new BurstFileWriter(file_path, max_event, sob_time, run_number, burst_data->burst_id));
						event_count = 1;
					}

					// Set SOB time in the event
					event->SOBtimestamp = sob_time;
					writer->writeEvent(event);
					delete event_message; // Delete zmq message

					//Updating statistics in bunches of 50
					if ((total_event_count % 50) == 0) {
						pool_burst_progress_[thread_id].count = total_event_count;
					}
				} else if (burst_data->timeDiff() > Options::GetInt(OPTION_TIMEOUT)) {
					if (burst_data.use_count() >= 2) { // it should be == 2
						// The resource is still hold by the TBB concurrent map first I need to detach it.
						// I cannot directly looking for they key index since it could have been recreated after a previos deletion  (short timeout, multiple files)
						BurstTable::accessor acc;
						bool found = merger_data_->table_.find(acc, burst_data->burst_id);
						if (found) {
							merger_data_->table_.erase(acc); //https://community.intel.com/t5/Intel-oneAPI-Threading-Building/concurrent-hash-map-erase-deadlock-when-using-erase-key-while/td-p/826829
							LOG_INFO("Erased " << burst_data->burst_id << " count " << burst_data.use_count());
							// Waiting for the next loop to close
						} else {
							LOG_ERROR("Something went really wrong.. " << burst_data->burst_id << " was not in the concurrent map.. ");
						}
						// Waiting for the next loop to close
					} else if (burst_data.use_count() == 1) {
						// Queue empty Timeout gone, Resource deleteted on the BustTable
						// I'm the only owner fo the resource 
						if (burst_data->event_queue.unsafe_size() > 0) {
							// The queue has still events keep on writing..
							continue;
						}
						// No event left exit the loop closing the file
						break;
					}
				} else {
					// No event popped
					// wait for the timeout expiration
					std::this_thread::sleep_for(std::chrono::milliseconds(20));
				}
			}
			// logic to close the file

			if ((event_count + burst_data->eob_packets.size()) > max_event) {
				LOG_ERROR("You are very unlucky.. the eob wont fit in the preallocated size of the file..");
			} else {
				for (auto & eob_packet: burst_data->eob_packets) {
					LOG_INFO("Processing EOB event...");
					try { 
						// write the event!					
						++event_count;
						++total_event_count;
						EVENT_HDR * eob_event = merger_data_->getEOBCollector().addEobDataToEvent(eob_packet);
						// Set SOB time in the event
						eob_event->SOBtimestamp = sob_time;
						writer->writeEvent(eob_event);
						delete eob_event; // free memory
					} catch (std::exception &exception) {
						LOG_ERROR("Missing DIM information for burst " << burst_data->burst_id
									<< " in run " <<  run_number << ". This burst will have missing SOB/EOB information!");
					} catch (...) {
						LOG_ERROR("Unknown exception raised fetching data from EOBCollector burst: " << burst_data->burst_id << " in run " <<  run_number);
					}
				}
			}

			LOG_INFO("Erasing DimInfo for current burst " << burst_data->burst_id);
			merger_data_->getEOBCollector().eraseBurstInfoSOB(burst_data->burst_id);
			merger_data_->getEOBCollector().eraseBurstInfoEOB(burst_data->burst_id);

			// Adding dim info to DIM info if any
			burst_data->eob_packets.clear();

			LOG_INFO("Writing header and footer of file: " << file_path);
			// Forcing the destructor to be called here!
			// Updating with the correct number of events written, this information will be flushed in the file when the destructor is called.
			//writer->setNumberOfEvents(event_count);
			writer->flushFile(event_count);
			writer.reset(); // Flushing the file

			//Updating statistics to the maximum value
			pool_burst_progress_[thread_id].count = total_event_count; // TODO why two time???

			try {
				if (BurstFileWriter::doChown(file_path, Options::GetString(OPTION_FILE_AUTHOR), Options::GetString(OPTION_FILE_GROUP))) {
					LOG_INFO("Correctly changed permissions to: " << file_path);
				} else {
					LOG_ERROR("Chown failed on file: " << file_path);
				}
			} catch (const std::exception & exception) {
				LOG_ERROR("Chown Failed " << exception.what() << " filename: " << file_path);
			}
		}
		// All Done here, Back to sleep
	}
	LOG_INFO("Thread Pool Stopping thread: " << thread_id);
}

void ThreadPool::stop() {
	LOG_INFO("Stopping write Pool");
    
    while (true) {
        if (not job_queue_.empty()) {
            mutex_.unlock(); // TODO Not sure why I need that..
			std::this_thread::sleep_for(std::chrono::seconds(1));
            LOG_INFO("Flushing the jobs queue.. number of burst to finish: " + job_queue_.unsafe_size());
            new_job_.notify_all();
            continue;
        }
        break;
    }
    is_running_ = false;
    new_job_.notify_all();
    for (auto & thread: threads_) {
        if (thread.joinable()) {
        	LOG_INFO("Thread: " << thread.get_id());
            thread.join();
        }
    }
}

ThreadPool::~ThreadPool() {
}

}
}
