/*
	2022, Ceoletta Marco
*/
#ifndef SYNCROMERGER_H_
#define SYNCROMERGER_H_

#include <atomic>
#include <thread>
#include <chrono>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include "../options/MyOptions.h"


#include <dic.hxx>

#include "MergerNoCache.h"

namespace na62 {
namespace merger {


class SyncroMerger: public MergerNoCache {
public:
	SyncroMerger();
	~SyncroMerger();

	class BurstInfo : public DimInfo {
	public:
		BurstInfo(SyncroMerger* g): DimInfo("RunControl/BurstTimeStruct",-1), owner(g) {
			BurstInfo::DelayedEobSignal.connect(boost::bind(&na62::merger::SyncroMerger::BurstInfo::delayedEndDataCollection, this));
		}

		/* // FAKE DIM INTERFACE USED FOR TESTING THE FUNCTIONALITY
		BurstInfo(SyncroMerger* g): owner(g) {
		fake_dim_ =  std::thread([&](){
       			while (true) {
					std::this_thread::sleep_for(std::chrono::seconds(4));
					std::cout << "In burst" << std::endl;
					setOB(true);
					std::this_thread::sleep_for(std::chrono::seconds(10));
					std::cout << "Outburst" << std::endl;
					setOB(false);
				}
     		});
		}
	*/
	~BurstInfo() {
		//fake_dim_.join(); // If the fake DIM thread is started the destructor needs to join it
	}
		
	void infoHandler() {
		uint dataLength = getSize();
	
		if (dataLength != sizeof(BurstTimeInfo)) {
			LOG_ERROR("EOBSyncro -- DIM Service " << getName() << " does not contain enough data: Found only "
				<< dataLength << " B but at least " << sizeof(BurstTimeInfo) << " B are needed to store the burst time information");
		} else {
			BurstTimeInfo* hdr = static_cast<BurstTimeInfo*>(getData());

			if(hdr->eobTime != 0) {//EOB
				LOG_INFO("EOB! -- delay for ending data collection started");
				DelayedEobSignal();
			} else {  //SOB 
				dataCollection(true);
				LOG_INFO("SOB! -- starting data collection");
			}
				
		} 
    }

	private:

	void delayedEndDataCollection() {
			std::this_thread::sleep_for(std::chrono::seconds(Options::GetInt(OPTION_EOB_DEAD_TIME)));
			dataCollection(false);
			LOG_INFO("data collection ended");
	}

	void dataCollection(bool st) const { //set
	  	owner->on_Data_coll_ = st;
	}

	//std::thread fake_dim_;

    SyncroMerger* const owner;
	boost::signals2::signal<void()> DelayedEobSignal;


	};

std::atomic<bool> on_Data_coll_;

private:
	void thread() override;
	BurstInfo* bi_;
};

} /* namespace merger */
} /* namespace na62 */
#endif /* SYNCROMERGER_H_ */
